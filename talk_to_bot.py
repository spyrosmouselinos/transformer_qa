import tensorflow as tf
from data_preprocess import pipeline
from train import transformer
from train import predict
from tensorflow.keras.models import Model

# Hyper-parameters
NUM_LAYERS = 2
D_MODEL = 256
NUM_HEADS = 8
UNITS = 512
DROPOUT = 0.1
MAX_LENGTH = 10

data, VOCAB_SIZE, START_TOKEN, END_TOKEN, tokenizer = pipeline(
    overload_questions=['Who is the best gurl?', 'Who is the best boi?', 'What is my name gurl?',
                        'What is my name boi?'],
    overload_answers=['Sofia is!', 'Spyritos is!', 'Sofia', 'Spyritos'])

model = transformer(
    vocab_size=VOCAB_SIZE,
    num_layers=NUM_LAYERS,
    units=UNITS,
    d_model=D_MODEL,
    num_heads=NUM_HEADS,
    dropout=DROPOUT)

model.load_weights('./simple_transformer.h5')

while True:
    question = input()
    print(predict(question))
    print("----\n")
